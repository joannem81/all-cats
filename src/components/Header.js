import React, { useState } from 'react';
import { ReactComponent as HomeIcon } from './../images/home.svg';
import { ReactComponent as Logo } from './../images/logo.svg';
import { ReactComponent as MenuIcon } from './../images/menu.svg';

const Header = () => {

  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <header className="header">
      <div className="header__content">
        <div className="header__logo">
          {/* Logo SVG must include a descriptive title element to ensure the content is accessible */}
          <Logo />
        </div>
        <nav className="header__nav">
          {/* Menu SVG is aria-hidden, use an aria-label to ensure button has an accessible name */}
          <button className="header__nav-button" aria-expanded={menuOpen} 
            onClick={() => setMenuOpen(!menuOpen)} aria-label="Menu">
            <MenuIcon />
          </button>
          {/* Navigation links must be immediately after button to ensure content order is correct for assistive technologies */}
          <ul className="header__nav-list" style={{ display: (menuOpen ? 'block' : 'none')}}>
            <li className="header__nav-item"><a className="header__nav-link" href="/">Cat delivery</a></li>
            <li className="header__nav-item"><a className="header__nav-link" href="/">Book a visit</a></li>
            <li className="header__nav-item"><a className="header__nav-link" href="/">Donate</a></li>
          </ul>
        </nav>
        {/* Home SVG is aria-hidden, use an aria-label to ensure link has an accessible name */}
        <a className="header__home" href="/" aria-label="Home">
          <HomeIcon />
        </a>
      </div>
    </header>
  )
}

export default Header;