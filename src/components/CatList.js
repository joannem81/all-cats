import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ReactComponent as Badge } from './../images/badge.svg';

const CatList = () => {

  const [catList, setCatList] = useState([]);
  const [errorMessage, setErrorMessage] = useState('');

  useEffect(() => {
    // test 'best' tags - mock API does not contain tags data
    // setCatList([{"id":"11","name":"Catrina","image":"https://placekitten.com/g/600/400","description":"Likes being playful and affectionate. Dislikes improve comedy","tags":['best']},{"id":"12","name":"Cleo-catra","image":"https://placekitten.com/g/600/400","description":"Likes being playful and affectionate. Dislikes improve comedy","tags":[]},{"id":"13","name":"Tony","image":"https://placekitten.com/g/600/400","description":"Likes staring out the window. Dislikes the warlocks curse that transformed him into a cat","tags":[]},{"id":"14","name":"Jeremy Corbyn","image":"https://placekitten.com/g/600/400","description":"Enjoys sleeping in the sun. Dislikes 15th century feudal Japan","tags":[]},{"id":"15","name":"Cat-man and Robin","image":"https://placekitten.com/g/600/400","description":"Likes cuddling with humans. Dislikes badminton","tags":[]},{"id":"16","name":"Tiger Woods","image":"https://placekitten.com/g/600/400","description":"Enjoys long walks on the beach","tags":[]},{"id":"17","name":"Smelly Cat","image":"https://placekitten.com/g/600/400","description":"What are they feeding you?","tags":[]},{"id":"18","name":"Earlene Wisozk","image":"https://placekitten.com/g/600/400","description":"The greatest","tags":[]},{"id":"19","name":"Prince Phillip","image":"https://placekitten.com/g/600/400","description":"International cat super start","tags":[]},{"id":"20","name":"Cardi B","image":"https://placekitten.com/g/600/400","description":"North Carolina","tags":[]},{"id":"21","name":"Leone Dooley","image":"http://lorempixel.com/640/480/abstract","description":"invoice Checking Account","tags":[]},{"id":"22","name":"Juanita Schulist","image":"http://lorempixel.com/640/480/nightlife","description":"Bedfordshire Home Loan Account Books","tags":[]},{"id":"23","name":"Arielle Schamberger","image":"http://lorempixel.com/640/480/city","description":"open-source Technician","tags":[]}]);
    
    // get mock data
    axios.get('https://5e5932cd7777050014463360.mockapi.io/cats')
      .then(res => {
        if(res.data){
          setCatList(res.data);
        } else {
          setErrorMessage('No cats found')
        }
      })
      .catch(() => setErrorMessage('Error retrieving data'))
  }, []);

  return (
    <>
      { catList.length ? (
        <ul className="cat-list">
        {
          catList.map(cat =>
            <li key={cat.id} className="cat-list__item">
              <div className="cat-list__image-area">
                { cat.tags.includes('best') &&
                  /* Badge SVG must include a descriptive title element to ensure the content is accessible */
                  <Badge />
                }
                <img src={cat.image} alt="" className="cat-list__image" />
              </div>
              <h2 className="cat-list__name">{cat.name}</h2>
              <p className="cat-list__text">{cat.description}</p>
              <a className="cat-list__link" href={`/cat/${cat.id}`}>Take home <span className="visually-hidden">{cat.name}</span></a>
            </li>
          )
        }
        </ul>
      )
      :
      (
        <p>Loading</p>
      )}
      { errorMessage !== '' && <p>{errorMessage}</p> }
    </>
  )
}

export default CatList;