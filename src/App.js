import React from 'react';
import Header from './components/Header';
import CatList from './components/CatList';
import './styles/_app.scss';

function App() {
  return (
    <>
      <a href='#main' className='skip-link'>Skip to main content</a>
      <Header />
      <main className="main" id="main">
        <h1 className="title1">Choose cats</h1>
        <CatList />
      </main>
    </>
  );
}

export default App;
